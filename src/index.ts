import { IProtAnnotConfig } from './interfaces';
import { ProtAnnot } from './ProtAnnot';

export { ProtAnnot, IProtAnnotConfig };
