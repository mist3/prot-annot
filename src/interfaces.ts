interface IProtAnnotConfig {
  pfam31?: boolean;
  tmhmm2?: boolean;
}

export { IProtAnnotConfig };
