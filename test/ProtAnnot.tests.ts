import { expect } from 'chai'
import { JSDOM } from 'jsdom'
import { IAseqMist3Api } from 'mist3-ts'

import { ProtAnnot } from '../src/ProtAnnot'

const emptyAseq: IAseqMist3Api = {
  agfam2: [],
  coils: [],
  ecf1: [],
  id: '',
  length: 0,
  pfam31: [],
  segs: [],
  sequence: '',
  stp: {version: null},
  tmhmm2: {},
}

describe('ProtAnnot', () => {
  describe('constructor', () => {
    it(`should respond with supported features`, () => {
      const protAnnot = new ProtAnnot(emptyAseq)
      const supportedFeatures = [
        'pfam31',
        'tmhmm2'
      ]
      expect(protAnnot.supportedFeatures).eql(supportedFeatures)
    })
  })
  describe('draw', () => {
    it('Should append an svg element in the HTML element passed as argument', () => {
      const { window } = new JSDOM(`<!DOCTYPE html><body><div id="prot-annot"/></body>`)
      const protAnnotDiv = window.document.getElementById('prot-annot')
      const config = {}
      const protAnnot = new ProtAnnot(emptyAseq, config)
      if (protAnnotDiv) {
        protAnnot.draw(protAnnotDiv)
        const div = window.document.getElementById('prot-annot')
        if (div) {
          expect(div.innerHTML).eql('<svg></svg>')
        }
      }
    })
    it('Should append an svg element with the sequence segment', () => {
      const { window } = new JSDOM(`<!DOCTYPE html><body><div id="prot-annot"/></body>`)
      const protAnnotDiv = window.document.getElementById('prot-annot')
      const config = {}
      const aseq = emptyAseq
      aseq.length = 200
      const protAnnot = new ProtAnnot(aseq, config)
      if (protAnnotDiv) {
        protAnnot.draw(protAnnotDiv)
        const div = window.document.getElementById('prot-annot')
        if (div) {
          expect(div.innerHTML).eql(
            '<svg height="30" width="100"><g class="prot-annot-seq"><rect id="prot-annot-seq-0" x="1" y="12.5" width="100" height="5" fill="gray" opacity="1"></rect></g></svg>'
          )
        }
      }
    })
    it('Should append an svg element with the sequence and TM segment', () => {
      const { window } = new JSDOM(`<!DOCTYPE html><body><div id="prot-annot"/></body>`)
      const protAnnotDiv = window.document.getElementById('prot-annot')
      const config = {}
      const aseq = emptyAseq
      aseq.length = 200
      aseq.tmhmm2 = {
        tms: [
          [10, 30],
          [100, 120]
        ]
      }
      const protAnnot = new ProtAnnot(aseq, config)
      if (protAnnotDiv) {
        protAnnot.draw(protAnnotDiv)
        const div = window.document.getElementById('prot-annot')
        if (div) {
          expect(div.innerHTML).eql(
            '<svg height="30" width="100"><g class="prot-annot-seq"><rect id="prot-annot-seq-0" x="1" y="12.5" width="100" height="5" fill="gray" opacity="1"></rect></g><g class="prot-annot-tm"><rect id="prot-annot-tm-0" x="5" y="0" width="10" height="30" fill="rgba(0,126,204,0.7)" opacity="0.7"></rect><rect id="prot-annot-tm-1" x="50" y="0" width="10" height="30" fill="rgba(0,126,204,0.7)" opacity="0.7"></rect></g></svg>'
          )
        }
      }
    })
    it('Should append an svg element with the sequence, TM and pfam domains', () => {
      const { window } = new JSDOM(`<!DOCTYPE html><body><div id="prot-annot"/></body>`)
      const protAnnotDiv = window.document.getElementById('prot-annot')
      const config = {}
      const aseq = emptyAseq
      aseq.length = 689
      aseq.tmhmm2.tms = [
        [
          7,
          29
        ],
        [
          300,
          322
        ]
      ]
      aseq.pfam31 = [
        {
          "name": "MCPsignal",
          "score": 163.1,
          "bias": 20.5,
          "c_evalue": 5.9e-52,
          "i_evalue": 5e-48,
          "hmm_from": 18,
          "hmm_to": 171,
          "hmm_cov": "..",
          "ali_from": 476,
          "ali_to": 629,
          "ali_cov": "..",
          "env_from": 468,
          "env_to": 630,
          "env_cov": "..",
          "acc": 0.98
        },
        {
          "name": "HAMP",
          "score": 35.4,
          "bias": 0,
          "c_evalue": 1.2e-12,
          "i_evalue": 9.8e-9,
          "hmm_from": 6,
          "hmm_to": 48,
          "hmm_cov": "..",
          "ali_from": 325,
          "ali_to": 368,
          "ali_cov": "..",
          "env_from": 323,
          "env_to": 369,
          "env_cov": "..",
          "acc": 0.93
        }
      ]
      const protAnnot = new ProtAnnot(aseq, config)
      if (protAnnotDiv) {
        protAnnot.draw(protAnnotDiv)
        const div = window.document.getElementById('prot-annot')
        if (div) {
          expect(div.innerHTML).eql(
            '<svg height="30" width="344.5"><g class="prot-annot-seq"><rect id="prot-annot-seq-0" x="1" y="12.5" width="344.5" height="5" fill="gray" opacity="1"></rect></g><g class="prot-annot-tm"><rect id="prot-annot-tm-0" x="3.5" y="0" width="11" height="30" fill="rgba(0,126,204,0.7)" opacity="0.7"></rect><rect id="prot-annot-tm-1" x="150" y="0" width="11" height="30" fill="rgba(0,126,204,0.7)" opacity="0.7"></rect></g><g class="prot-annot-pfam"><g id="prot-annot-pfam-domain-0"><rect id="prot-annot-pfam-body-0" class="prot-annot-pfam-body" x="162.5" y="2.5" width="21.5" height="25" fill="white" opacity="0.9" stroke="black" title="HAMP" stroke-width="0"></rect><path d=" M 162.5 2.5 L 184 2.5 L 182.5 8.75 L 184 15 L 182.5 21.25 L 184 27.5 L 162.5 27.5 L 164 21.25 L 162.5 15 L 164 8.75 L 162.5 2.5 Z" stroke="black" stroke-width="2" fill="none" stroke-linecap="round" title="HAMP" id="prot-annot-pfam-borders"></path><text x="173.25" y="15" text-anchor="middle" alignment-baseline="central" textLength="11.5" font-family="Verdana" font-size="15" lengthAdjust="spacingAndGlyphs" title="HAMP" class="prot-annot-pfam-names" id="prot-annot-pfam-name-0">HAMP</text></g><g id="prot-annot-pfam-domain-1"><rect id="prot-annot-pfam-body-1" class="prot-annot-pfam-body" x="238" y="2.5" width="76.5" height="25" fill="white" opacity="0.9" stroke="black" title="MCPsignal" stroke-width="0"></rect><path d=" M 238 2.5 L 314.5 2.5 L 313 8.75 L 314.5 15 L 313 21.25 L 314.5 27.5 L 238 27.5 L 239.5 21.25 L 238 15 L 239.5 8.75 L 238 2.5 Z" stroke="black" stroke-width="2" fill="none" stroke-linecap="round" title="MCPsignal" id="prot-annot-pfam-borders"></path><text x="276.25" y="15" text-anchor="middle" alignment-baseline="central" textLength="66.5" font-family="Verdana" font-size="15" lengthAdjust="spacingAndGlyphs" title="MCPsignal" class="prot-annot-pfam-names" id="prot-annot-pfam-name-1">MCPsignal</text></g></g></svg>'
          )
        }
      }
    })
  })
  describe('Edge cases', () => {
    it('should remove overlaps', () => {
      const aseqWithOverlap = {
        "pfam31": [
          {
            "name": "MCPsignal",
            "score": 144.6,
            "bias": 22.9,
            "c_evalue": 7.2e-46,
            "i_evalue": 2.4e-42,
            "hmm_from": 6,
            "hmm_to": 170,
            "hmm_cov": "..",
            "ali_from": 323,
            "ali_to": 501,
            "ali_cov": "..",
            "env_from": 316,
            "env_to": 503,
            "env_cov": "..",
            "acc": 0.97
          },
          {
            "name": "sCache_2",
            "score": 97.2,
            "bias": 0,
            "c_evalue": 2.3e-31,
            "i_evalue": 7.8e-28,
            "hmm_from": 4,
            "hmm_to": 151,
            "hmm_cov": "..",
            "ali_from": 42,
            "ali_to": 182,
            "ali_cov": "..",
            "env_from": 39,
            "env_to": 185,
            "env_cov": "..",
            "acc": 0.93
          },
          {
            "name": "dCache_2",
            "score": 91.4,
            "bias": 0,
            "c_evalue": 1.6e-29,
            "i_evalue": 5.4e-26,
            "hmm_from": 37,
            "hmm_to": 195,
            "hmm_cov": "..",
            "ali_from": 34,
            "ali_to": 186,
            "ali_cov": "..",
            "env_from": 27,
            "env_to": 191,
            "env_cov": "..",
            "acc": 0.93
          },
          {
            "name": "Cache_3-Cache_2",
            "score": 38.4,
            "bias": 0,
            "c_evalue": 2.3e-13,
            "i_evalue": 7.8e-10,
            "hmm_from": 176,
            "hmm_to": 285,
            "hmm_cov": "..",
            "ali_from": 66,
            "ali_to": 179,
            "ali_cov": "..",
            "env_from": 48,
            "env_to": 182,
            "env_cov": "..",
            "acc": 0.85
          },
          {
            "name": "HAMP",
            "score": 37.6,
            "bias": 0,
            "c_evalue": 6.3e-13,
            "i_evalue": 2.1e-9,
            "hmm_from": 1,
            "hmm_to": 42,
            "hmm_cov": "[.",
            "ali_from": 207,
            "ali_to": 249,
            "ali_cov": "..",
            "env_from": 207,
            "env_to": 256,
            "env_cov": "..",
            "acc": 0.89
          }
        ],
        "agfam2": [],
        "ecf1": [],
        "stp": {
          "ranks": [
            "chemotaxis",
            "mcp",
            "40H"
          ],
          "counts": {
            "HAMP": 1,
            "dCache_2": 1,
            "MCPsignal": 1
          },
          "inputs": [
            "dCache_2"
          ],
          "cheHits": [
            {
              "name": "mcp-40H",
              "score": 382,
              "bias": 71.6,
              "c_evalue": 2.3e-117,
              "i_evalue": 1.9e-116,
              "hmm_from": 5,
              "hmm_to": 281,
              "hmm_cov": ".]",
              "ali_from": 260,
              "ali_to": 536,
              "ali_cov": ".]",
              "env_from": 243,
              "env_to": 536,
              "env_cov": ".]",
              "acc": 0.99
            },
            {
              "name": "mcp-44H",
              "score": 195.3,
              "bias": 52.4,
              "c_evalue": 7.2e-61,
              "i_evalue": 5.8e-60,
              "hmm_from": 23,
              "hmm_to": 264,
              "hmm_cov": "..",
              "ali_from": 264,
              "ali_to": 505,
              "ali_cov": "..",
              "env_from": 262,
              "env_to": 511,
              "env_cov": "..",
              "acc": 0.98
            },
            {
              "name": "mcp-24H",
              "score": 167.1,
              "bias": 24,
              "c_evalue": 2.7e-52,
              "i_evalue": 2.2e-51,
              "hmm_from": 20,
              "hmm_to": 153,
              "hmm_cov": "..",
              "ali_from": 331,
              "ali_to": 464,
              "ali_cov": "..",
              "env_from": 322,
              "env_to": 478,
              "env_cov": "..",
              "acc": 0.96
            },
            {
              "name": "mcp-52H",
              "score": 110,
              "bias": 49.9,
              "c_evalue": 3.8e-35,
              "i_evalue": 3e-34,
              "hmm_from": 64,
              "hmm_to": 279,
              "hmm_cov": "..",
              "ali_from": 270,
              "ali_to": 492,
              "ali_cov": "..",
              "env_from": 255,
              "env_to": 500,
              "env_cov": "..",
              "acc": 0.84
            },
            {
              "name": "mcp-36H",
              "score": 108.6,
              "bias": 0.7,
              "c_evalue": 8.3e-35,
              "i_evalue": 6.7e-34,
              "hmm_from": 96,
              "hmm_to": 146,
              "hmm_cov": "..",
              "ali_from": 365,
              "ali_to": 415,
              "ali_cov": "..",
              "env_from": 364,
              "env_to": 428,
              "env_cov": "..",
              "acc": 0.91
            }
          ],
          "outputs": [],
          "version": 1
        },
        "id": "L1ApXvv4UKvYXsaXSvis7Q",
        "length": 536,
        "sequence": "MLKLENIKVSYKLAAIVIVGIVGFLSLLFISANALKENLVAEREARLKAVIQSTLSQVAYLNQTLPKEQAQEQAKALINALRFDGNNYMFVIDESRYTVVHPIRQDLVGQQMGNPGKDTEGQFWFTMIDLARNGQQGSLIYPWKNQQGNPADKLSFVNGFAPWGWILGSGMLLDDIEHAVYQQFLRMGFATLIVTLVMIGLGVVISRAVIQPLDTIKDVMKKVAQGDLTAQIPVLGKDELGIVAQRINNSIAAVHDALVESVQSASSVAEAAIRIASSAEETSQAVVSQRDQLSQLATAMNQMTATVADVAGHAEDTARDTLDASKEANLGDKDVHSSVDSIRALSVELGVATDQVNKLKEGVMQISEVTSVISGISEQTNLLALNAAIEAARAGDQGRGFAVVADEVRNLASRTHHSTDEIQTMINRLQQLAVSTASAMQKSQALAANSVATAENAGSDLSLIVNHIQHVSDKATQIATAAEEQSAVAEEMNRNVSGINDAALEMSQAATYLAEESEKLADLSRQLDQKLTAFKL",
        "segs": [
          [
            259,
            283
          ],
          [
            382,
            394
          ]
        ],
        "coils": [
          [
            513,
            533
          ]
        ],
        "tmhmm2": {
          "tms": [
            [
              13,
              35
            ],
            [
              154,
              173
            ],
            [
              188,
              210
            ]
          ],
          "topology": [
            [
              "o",
              1,
              12
            ],
            [
              "M",
              13,
              35
            ],
            [
              "i",
              36,
              153
            ],
            [
              "M",
              154,
              173
            ],
            [
              "o",
              174,
              187
            ],
            [
              "M",
              188,
              210
            ],
            [
              "i",
              211,
              536
            ]
          ]
        }
      }
      const protAnnot = new ProtAnnot(aseqWithOverlap, {})
      const { window } = new JSDOM(`<!DOCTYPE html><body><div id="prot-annot"/></body>`)
      const protAnnotDiv = window.document.getElementById('prot-annot')
      if (protAnnotDiv) {
        protAnnot.draw(protAnnotDiv)
        const div = window.document.getElementById('prot-annot')
        if (div) {
          expect(div.innerHTML).eql(
            '<svg height="30" width="268"><g class="prot-annot-seq"><rect id="prot-annot-seq-0" x="1" y="12.5" width="268" height="5" fill="gray" opacity="1"></rect></g><g class="prot-annot-tm"><rect id="prot-annot-tm-0" x="6.5" y="0" width="11" height="30" fill="rgba(0,126,204,0.7)" opacity="0.7"></rect><rect id="prot-annot-tm-1" x="77" y="0" width="9.5" height="30" fill="rgba(0,126,204,0.7)" opacity="0.7"></rect><rect id="prot-annot-tm-2" x="94" y="0" width="11" height="30" fill="rgba(0,126,204,0.7)" opacity="0.7"></rect></g><g class="prot-annot-pfam"><g id="prot-annot-pfam-domain-0"><rect id="prot-annot-pfam-body-0" class="prot-annot-pfam-body" x="21" y="2.5" width="70" height="25" fill="white" opacity="0.9" stroke="black" title="sCache_2" stroke-width="0"></rect><path d=" M 21 2.5 L 91 2.5 L 89.5 8.75 L 91 15 L 89.5 21.25 L 91 27.5 L 21 27.5 L 22.5 21.25 L 21 15 L 22.5 8.75 L 21 2.5 Z" stroke="black" stroke-width="2" fill="none" stroke-linecap="round" title="sCache_2" id="prot-annot-pfam-borders"></path><text x="56" y="15" text-anchor="middle" alignment-baseline="central" textLength="60" font-family="Verdana" font-size="15" lengthAdjust="spacingAndGlyphs" title="sCache_2" class="prot-annot-pfam-names" id="prot-annot-pfam-name-0">sCache_2</text></g><g id="prot-annot-pfam-domain-1"><rect id="prot-annot-pfam-body-1" class="prot-annot-pfam-body" x="103.5" y="2.5" width="21" height="25" fill="white" opacity="0.9" stroke="black" title="HAMP" stroke-width="0"></rect><path d=" M 103.5 2.5 L 124.5 2.5 L 123 8.75 L 124.5 15 L 123 21.25 L 124.5 27.5 L 103.5 27.5 L 103.5 21.25 L 103.5 15 L 103.5 8.75 L 103.5 2.5 Z" stroke="black" stroke-width="2" fill="none" stroke-linecap="round" title="HAMP" id="prot-annot-pfam-borders"></path><text x="114" y="15" text-anchor="middle" alignment-baseline="central" textLength="11" font-family="Verdana" font-size="15" lengthAdjust="spacingAndGlyphs" title="HAMP" class="prot-annot-pfam-names" id="prot-annot-pfam-name-1">HAMP</text></g><g id="prot-annot-pfam-domain-2"><rect id="prot-annot-pfam-body-2" class="prot-annot-pfam-body" x="161.5" y="2.5" width="89" height="25" fill="white" opacity="0.9" stroke="black" title="MCPsignal" stroke-width="0"></rect><path d=" M 161.5 2.5 L 250.5 2.5 L 249 8.75 L 250.5 15 L 249 21.25 L 250.5 27.5 L 161.5 27.5 L 163 21.25 L 161.5 15 L 163 8.75 L 161.5 2.5 Z" stroke="black" stroke-width="2" fill="none" stroke-linecap="round" title="MCPsignal" id="prot-annot-pfam-borders"></path><text x="206" y="15" text-anchor="middle" alignment-baseline="central" textLength="79" font-family="Verdana" font-size="15" lengthAdjust="spacingAndGlyphs" title="MCPsignal" class="prot-annot-pfam-names" id="prot-annot-pfam-name-2">MCPsignal</text></g></g></svg>'
          )
        }
      }
    })
    it('should work here too', () => {
      const aseqWithOverlap = {"pfam31":[{"name":"FHIPEP","score":895.5,"bias":6.3,"c_evalue":1.3e-273,"i_evalue":2.1e-269,"hmm_from":2,"hmm_to":657,"hmm_cov":"..","ali_from":31,"ali_to":689,"ali_cov":"..","env_from":30,"env_to":690,"env_cov":"..","acc":0.97}],"agfam2":[],"ecf1":[],"stp":{"version":null},"id":"ZsU-734LRms_bAeGpnV_wA","length":700,"sequence":"MGFKAGLGRFKEYRWLLTKGVGTPLLVLASLGMVVLPIPPLMLDILFSFNIALSIVVLLVAVYTRRPLEFAAFPTVLLIATLLRLALNVASTRVVLLEGHNGTAAAGHVIEAFGNVVIGGNYAVGIIVFMILVIINFVVVTKGAGRISEVSARFTLDAMPGKQMAIDADLNAGLINQEEAKKRRQDVTQEADFYGSMDGASKFVKGDAVAGIMILFINIIGGFIIGMVQHQLTFGEAAEVYTLLAIGDGLVAQIPSLLLSIAAAIIVTRQNTDQDMGTAVLGQLFENPKALVISAGILLMMGSVPGMPHLAFLSLGACAAVGAWWLLRREKQNKARAASGELLPAHGDAPVEQKDLSWDDVMPVDIIGLEVGYQLIPLVDKSQGGELLNRIKGVRKKLSQEFGFLVPAVHIRDNLDLAPNQYRITLMGVSTGEATVYHDKEMAINPGQVFGQIQGIATQDPAFGLEAVWVAKEQVSQAQTLGYTVVDAATVVATHMSQILSNHAALLLGHDEVQQLLDMIGKHQSKLVEGLVPEVISMGNLVKVLQNLLNEGVPIRDMRTILQTLVEYAPRSPDPEVLTAACRIALRRLIVQEIAGPEPELPVITLSPELERILHQSLQAGGGDGAGIEPGLAERMQRSLVEATQRQELEGQPAVLLTSGILRNTLAKFVKNAIPGLRVLSYQEVPDDKQIRIVSAVGQN","segs":[[51,62],[77,87],[211,227],[484,494],[620,633]],"coils":[],"tmhmm2":null}
      const protAnnot = new ProtAnnot(aseqWithOverlap, {})
      const { window } = new JSDOM(`<!DOCTYPE html><body><div id="prot-annot"/></body>`)
      const protAnnotDiv = window.document.getElementById('prot-annot')
      if (protAnnotDiv) {
        protAnnot.draw(protAnnotDiv)
        const div = window.document.getElementById('prot-annot')
        if (div) {
          expect(div.innerHTML).eql(
            '<svg height="30" width="350"><g class="prot-annot-seq"><rect id="prot-annot-seq-0" x="1" y="12.5" width="350" height="5" fill="gray" opacity="1"></rect></g><g class="prot-annot-pfam"><g id="prot-annot-pfam-domain-0"><rect id="prot-annot-pfam-body-0" class="prot-annot-pfam-body" x="15.5" y="2.5" width="329" height="25" fill="white" opacity="0.9" stroke="black" title="FHIPEP" stroke-width="0"></rect><path d=" M 15.5 2.5 L 344.5 2.5 L 343 8.75 L 344.5 15 L 343 21.25 L 344.5 27.5 L 15.5 27.5 L 17 21.25 L 15.5 15 L 17 8.75 L 15.5 2.5 Z" stroke="black" stroke-width="2" fill="none" stroke-linecap="round" title="FHIPEP" id="prot-annot-pfam-borders"></path><text x="180" y="15" text-anchor="middle" alignment-baseline="central" textLength="50" font-family="Verdana" font-size="15" lengthAdjust="spacingAndGlyphs" title="FHIPEP" class="prot-annot-pfam-names" id="prot-annot-pfam-name-0">FHIPEP</text></g></g></svg>'
          )
        }
      }
    })
  })
})