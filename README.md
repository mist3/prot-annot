# ProtAnnot

![npm](https://img.shields.io/npm/v/prot-annot.svg)
![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-blue.svg)
[![pipeline status](https://gitlab.com/mist3/prot-annot/badges/master/pipeline.svg)](https://gitlab.com/mist3/prot-annot/commits/master)
[![coverage report](https://gitlab.com/mist3/prot-annot/badges/master/coverage.svg)](https://mist3.gitlab.io/prot-annot/coverage)

A package to generate SVG images of protein annotation.

Heavily inspired by Vadim Gumerov's file [`draw-protein-feature.ts`](https://github.com/ToshkaDev/mist-web-v/blob/developer/src/app/core/common/drawSvg/draw-protein-feature.ts) in the [MiST3](https://mistdb.com) web interface.

